#include "mex.h"

#ifndef HAVE_OCTAVE
	#include "matrix.h"
#endif

#include "betacf.h"

void mexFunction(
	  int nlhs, mxArray*	plhs[]
	, int nrhs, const mxArray* prhs[]
)
{

	if ( nrhs != 3 )
	{
		mexErrMsgIdAndTxt( "betacf:NumberOfInputs", "The number of inputs must be exactly 3.\n" );
	}

	if ( nlhs > 1 )
	{
		mexErrMsgIdAndTxt( "betacf:NumberOfOutputs", "The number of outputs must be exactly 1.\n" );
	}

	auto const A = prhs[0];
	auto const B = prhs[1];
	auto const X = prhs[2];

	auto a = mxGetPr( A );
	auto na = mxGetNumberOfElements( A );

	auto b = mxGetPr( B );
	auto nb = mxGetNumberOfElements( B );

	auto x = mxGetPr( X );
	auto nx = mxGetNumberOfElements( X );


	auto S = A;
	auto ns = na;
	if ( ns < nb )	{ S = B; ns = nb; }
	if ( ns < nx )	{ S = X; ns = nx; }


	plhs[0] = mxCreateNumericArray( mxGetNumberOfDimensions( S ), mxGetDimensions( S ), mxDOUBLE_CLASS, mxREAL );
	auto y = mxGetPr( plhs[0] );
	auto yEnd = y + ns;

	int inca = ( na < ns ) ? 0 : 1;
	int incb = ( nb < ns ) ? 0 : 1;
	int incx = ( nx < ns ) ? 0 : 1;


	while( yEnd != y )
	{
		*y++ = PROJECT::math::numeric::betacf( *a, *b, *x );
		a += inca;
		b += incb;
		x += incx;
	}

}
