#include "mex.h"

#ifndef HAVE_OCTAVE
	#include "matrix.h"
#endif

// #include "gammln.h"
#include <cmath>

void mexFunction(
	  int nlhs, mxArray*	plhs[]
	, int nrhs, const mxArray* prhs[]
)
{

	if ( nrhs != 1 )
	{
		mexErrMsgIdAndTxt( "gammln:NumberOfInputs", "The number of inputs must be exactly 1.\n" );
	}

	if ( nlhs > 1 )
	{
		mexErrMsgIdAndTxt( "gammln:NumberOfOutputs", "The number of outputs must be exactly 1.\n" );
	}

	auto const X = prhs[0];
	auto x = mxGetPr( X );
	auto const xEnd = x + mxGetNumberOfElements( X );


	plhs[0] = mxCreateNumericArray( mxGetNumberOfDimensions( X ), mxGetDimensions( X ), mxDOUBLE_CLASS, mxREAL );
	auto y = mxGetPr( plhs[0] );

	while( xEnd != x )
		*y++ = std::lgamma( *x++ );
		// *y++ = PROJECT::math::numeric::gammln( *x++ );

}
