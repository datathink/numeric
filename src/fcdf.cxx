#include "fcdf.hxx"

#include "open_namespace"


template
float	fcdf( const float t, const float v1, const float v2 );

template
double	fcdf( const double t, const double v1, const double v2 );


#include "close_namespace"
