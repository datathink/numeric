#include "tcdf.hxx"


#include "open_namespace"


template
float	tcdf( const float t, const float v );

template
double	tcdf( const double t, const double v );


#include "close_namespace"
