#include "betai.hxx"

#include "open_namespace"

template
float betai( const float a, const float b, const float x );

template
double betai( const double a, const double b, const double x );


#include "close_namespace"
