#include "gammln.hxx"

#include "open_namespace"

template
float	gammln(const float xx);

template
double	gammln(const double xx);


#include "close_namespace"
