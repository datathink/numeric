#include "betacf.hxx"

#include "open_namespace"

template
float	betacf( const float a, const float b, const float x );

template
double	betacf( const double a, const double b, const double x );


#include "close_namespace"
