#!/bin/bash

if [ $# -lt 3 ]
then
	echo $0 SourceDir BuildDir InstallDir
	exit 1
fi

Source=$( readlink -e "$1" )
echo Source=$Source
Build="$2"
echo Build=$Build
Install="$( readlink -m $3 )"
echo Install=$Install
shift
shift
shift

mkdir -p "$Build"
cd "$Build"

cmake "$Source" -DCMAKE_INSTALL_PREFIX="$Install" -DCMAKE_CXX_COMPILER=g++-4.8 $*
