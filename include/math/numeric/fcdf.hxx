#ifndef __PROJECT__math__numeric__fcdf_hxx
#define __PROJECT__math__numeric__fcdf_hxx

#include "fcdf.h"
#include "betai.h"
#include <cassert>


#include "open_namespace"


template<typename T>
T	fcdf( const T f, const T v1, const T v2 ) {

	constexpr T zero = (T) 0.0;

	assert( zero < v1 );
	assert( zero < v2 );
	assert( zero <= f );


	if ( zero == f ) return zero;

	constexpr T half = (T) 0.5;

	const T	v1f = v1 * f;

	return betai( v1 * half, v2 * half, v1f/(v1f+v2) );
	
}


#include "close_namespace"


#endif // #ifndef __PROJECT__math__numeric__fcdf_hxx
