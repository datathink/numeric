#ifndef __PROJECT__math__numeric__betai_h
#define __PROJECT__math__numeric__betai_h


#include "open_namespace"

template<typename T>
T betai( const T a, const T b, const T x );

extern template
float betai( const float a, const float b, const float x );

extern template
double betai( const double a, const double b, const double x );


#include "close_namespace"


#endif // #ifndef __PROJECT__math__numeric__betai_h
