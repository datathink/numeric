#ifndef __PROJECT__math__numeric__fcdf_h
#define __PROJECT__math__numeric__fcdf_h


#include "open_namespace"


template<typename T>
T	fcdf( const T f, const T v1, const T v2 );

extern template
float	fcdf( const float f, const float v1, const float v2 );

extern template
double	fcdf( const double f, const double v1, const double v2 );


#include "close_namespace"


#endif // #ifndef __PROJECT__math__numeric__fcdf_h
