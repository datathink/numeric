#ifndef  __PROJECT__math__numeric__betacf_h
#define  __PROJECT__math__numeric__betacf_h


#include "open_namespace"


template<typename T>
T	betacf( const T a, const T b, const T x );

extern template
float	betacf( const float a, const float b, const float x );

extern template
double	betacf( const double a, const double b, const double x );


#include "close_namespace"


#endif  // __PROJECT__math__numeric__betacf_h
