#ifndef __PROJECT__math__numeric__betai_hxx
#define __PROJECT__math__numeric__betai_hxx


#include "betai.h"
#include "betacf.h"
// #include "gammln.h"


#include <cmath>


#include "open_namespace"

template<typename T>
T	betai( const T a, const T b, const T x )
{

	constexpr T zero = (T) 0.0;
	constexpr T one = (T) 1.0;
	constexpr T two = (T) 2.0;

	if ( x <= zero ) return zero;

	if ( x >= one ) return one;

	using std::exp;
	using std::log;

	// T const bt=exp(gammln(a+b)-gammln(a)-gammln(b)+a*log(x)+b*log(one-x));
	T const bt=exp(std::lgamma(a+b)-std::lgamma(a)-std::lgamma(b)+a*log(x)+b*log(one-x));

	if (x < (a+one)/(a+b+two))
		return bt*betacf(a,b,x)/a;
	else
		return one-bt*betacf(b,a,one-x)/b;
}

#include "close_namespace"


#endif // #ifndef __PROJECT__math__numeric__betai_hxx
