#ifndef __PROJECT__math__numeric__tcdf_hxx
#define __PROJECT__math__numeric__tcdf_hxx


#include "tcdf.h"
#include "betai.h"
#include <cmath>
#include <cassert>

#include "open_namespace"


template<typename T>
T	tcdf( const T t, const T v ) {

	constexpr T zero = (T) 0.0;
	constexpr T half = (T) 0.5;
	constexpr T one = (T) 1.0;

	assert( v > zero );

	constexpr T	gaussian_threshold = (T) 1.0e7;

	bool const	is_t_positive = t > zero;

	T	cdf = half;

	if ( one == v )			// Cauchy distribution
	{
		constexpr T pi = (T) 3.14159265359;
		cdf = std::atan2( one, -t ) / pi;	// Tested, correct.
	}
	else if ( gaussian_threshold < v )
	{
		// Approximate with Gaussian distribution
		static const T sqrt_half = std::sqrt( half );
		cdf = half + half * std::erf( t * sqrt_half );	// Tested, correct
	}
	else
	{
		/*
			Tested.
			The formula is correct.
			ToDo: improve accuracy when abs(t) is large.
		*/

		T const		tt = t * t;

		/*
			if t > 0
				cdf = 1 - 0.5 * betai( v * 0.5, 0.5, v / (v+t*t) )
				    = 1 - 0.5 * [ 1.0 - betai( 0.5, v * 0.5, t*t / (v+t*t) ) ] // Do not use a positive number subtract anohter positive number
				    = 0.5 + 0.5 * betai( 0.5, v * 0.5, t*t / (v+t*t) )
			else
				cdf = 0.5 * betai( v * 0.5, 0.5, v / (v+tt) )
		*/

		T const		half_v = v * half;

		cdf = is_t_positive ?
			  half + half * betai( half, half_v, tt/(v+tt) )
			: half * betai( half_v, half, v/(v+tt) )
			;

	}

	if ( zero == t )
		cdf = half;

	return cdf;

}


#include "close_namespace"


#endif // #ifndef __PROJECT__math__numeric__tcdf_hxx
