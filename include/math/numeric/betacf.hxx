#ifndef  __PROJECT__math__numeric__betacf_hxx
#define  __PROJECT__math__numeric__betacf_hxx


#include "betacf.h"

#include <cmath>
#include <limits>
#include <cassert>


#include "open_namespace"

template<typename T>
T	betacf(const T a, const T b, const T x)
{
	using std::numeric_limits;
	using std::fabs;

	constexpr int MAXIT=100;
	constexpr T EPS=numeric_limits<T>::epsilon();
	constexpr T FPMIN=numeric_limits<T>::min()/EPS;
	int m,m2;
	T aa,c,d,del,h,qab,qam,qap;

	qab=a+b;
	qap=a+1.0;
	qam=a-1.0;
	c=1.0;
	d=1.0-qab*x/qap;
	if (fabs(d) < FPMIN) d=FPMIN;
	d=1.0/d;
	h=d;
	for (m=1;m<=MAXIT;m++) {
		m2=2*m;
		aa=m*(b-m)*x/((qam+m2)*(a+m2));
		d=1.0+aa*d;
		if (fabs(d) < FPMIN) d=FPMIN;
		c=1.0+aa/c;
		if (fabs(c) < FPMIN) c=FPMIN;
		d=1.0/d;
		h *= d*c;
		aa = -(a+m)*(qab+m)*x/((a+m2)*(qap+m2));
		d=1.0+aa*d;
		if (fabs(d) < FPMIN) d=FPMIN;
		c=1.0+aa/c;
		if (fabs(c) < FPMIN) c=FPMIN;
		d=1.0/d;
		del=d*c;
		h *= del;
		if (fabs(del-1.0) <= EPS) break;
	}
	assert( m <= MAXIT );
	return h;
}

#include "close_namespace"


#endif // #ifndef  __PROJECT__math__numeric__betacf_hxx
