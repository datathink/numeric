#ifndef __PROJECT__math__numeric__tcdf_h
#define __PROJECT__math__numeric__tcdf_h


#include "open_namespace"


template<typename T>
T	tcdf( const T t, const T v );

extern template
float	tcdf( const float t, const float v );

extern template
double	tcdf( const double t, const double v );


#include "close_namespace"


#endif // #ifndef __PROJECT__math__numeric__tcdf_h
