#ifndef __PROJECT__math__numeric__gammln_h
#define __PROJECT__math__numeric__gammln_h

#include "open_namespace"

template<typename T>
T	gammln(const T xx);

extern template
float	gammln(const float xx);

extern template
double	gammln(const double xx);


#include "close_namespace"


#endif // #ifndef __PROJECT__math__numeric__gammln_h
