FUNCTION( Set_numeric VarPrefix BuildShared BuildStatic )


SET( ModuleName numeric )


IF( ${BuildShared} )
	SET( ${VarPrefix}_LIBRARIES ${ModuleName}_SHARED PARENT_SCOPE )
	SET( ${VarPrefix}_OBJECTS PARENT_SCOPE )
ELSEIF( ${BuildStatic} )
	SET( ${VarPrefix}_LIBRARIES ${ModuleName}_STATIC PARENT_SCOPE )
	SET( ${VarPrefix}_OBJECTS PARENT_SCOPE )
ELSE( ${BuildStatic} )
	SET( ${VarPrefix}_LIBRARIES PARENT_SCOPE )
	SET( ${VarPrefix}_OBJECTS $<TARGET_OBJECTS:${ModuleName}_OBJECT> PARENT_SCOPE )
ENDIF( ${BuildShared} )


ENDFUNCTION( Set_numeric )
