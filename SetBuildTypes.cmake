FUNCTION( SetBuildTypes VarPrefix BuildShared BuildStatic )


SET( ${VarPrefix}_BUILD_SHARED ${BuildShared} PARENT_SCOPE )
SET( ${VarPrefix}_BUILD_STATIC ${BuildStatic} PARENT_SCOPE )


IF( ${BuildShared} )
	SET( ${VarPrefix}_LIBRARIES ${VarPrefix}_SHARED PARENT_SCOPE )
	SET( ${VarPrefix}_OBJECTS PARENT_SCOPE )
ELSEIF( ${BuildStatic} )
	SET( ${VarPrefix}_LIBRARIES ${VarPrefix}_STATIC PARENT_SCOPE )
	SET( ${VarPrefix}_OBJECTS PARENT_SCOPE )
ELSE( ${BuildStatic} )
	SET( ${VarPrefix}_LIBRARIES PARENT_SCOPE )
	SET( ${VarPrefix}_OBJECTS $<TARGET_OBJECTS:${VarPrefix}_OBJECT> PARENT_SCOPE )
ENDIF( ${BuildShared} )


ENDFUNCTION( SetBuildTypes )
